package fr.inrialpes.exmo.stream;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.shared.PrefixMapping;

public class EngineQuery {
	/**
	 * It make a query to a model
	 * @param String q query
	 * @param Model m Model
	 * @return
	 */
	public static ResultSet SparqlQueryToModel(String q, Model m) {
        Query query = QueryFactory.create();
        query.setPrefixMapping(PrefixMapping.Standard);
        QueryFactory.parse(query, q, "", Syntax.syntaxSPARQL_11);
        QueryExecution qexec = QueryExecutionFactory.create(query, m);
        ResultSet r = qexec.execSelect();
        return r;
	}
	
	/**
	 * It makes a query to SPARQL Endpoint
	 * @param String q query
	 * @param String endpoint SPARQL endpoint
	 * @return
	 */
	public static ResultSet SparqlQueryToEndpoint(String q, String endpoint) {
        Query query = QueryFactory.create();
        query.setPrefixMapping(PrefixMapping.Standard);
        QueryFactory.parse(query, q, "", Syntax.syntaxSPARQL_11);
        QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
        ResultSet r = qexec.execSelect();
        return r;
	}
}
