package fr.inrialpes.exmo.stream;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.util.URIref;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
/**
 * 
 * @author asanchez75
 *
 */
public class RdfsSubClassOfTriple implements Triple {
	
	String pattern = "";
	String graph = "";
    String endpoint = "";
    Model model = null;
    Double op = 1.0;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(RdfsSubClassOfTriple.class);
    
	public RdfsSubClassOfTriple() {
		this.pattern = "SELECT ?class ?p ?superclass {\n" + 
				"?class ?p ?superclass .\n" + 
				"FILTER (?p = rdfs:subClassOf)" + 				
				"}\n";
	}

	@Override
	public Model getExtraTriples() {
		// not need to retrive extra triples
		return null;
	}
	
	@Override
	public String getPattern() {
		return this.pattern;
	}

	@Override
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public void setGraph(String graph) {
		this.graph = graph;
	}

	@Override
	public String getGraph() {
		return this.graph;
	}

	@Override
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	@Override
	public String getEndpoint() {
		return this.endpoint;
	}

	@Override
	public void setModel() {
		Model m = ModelFactory.createDefaultModel(); 
		this.model = m;
	}
	
	public void setModel(Model m) {
		this.model = m;
	}
	
	@Override
	public Model getModel() {
		return this.model;
	}
	
	public void setOp(Double op) {
		this.op = op;
	}
	
	public Double getOp(){
		return this.op;
	}
	
	@Override
	public HashMap<String, HashMap<String, Double>> computeCardinalities() {
		HashMap<String, HashMap<String, Double>> cardinalities = new HashMap<String, HashMap<String, Double>>();
		ArrayList<String> classes = new ArrayList<String>();

		String q = "SELECT ?class ?count ?links {\n" + 
				"{SELECT (count(?instance) as ?count) ?class {\n" + 
				"?class rdfs:subClassOf* ?superclass.\n" + 
				"?instance rdf:type ?class.\n" + 
				"FILTER (?superclass = <classuri>)\n" + 
				"} GROUP BY ?class}\n" + 
				"OPTIONAL\n" + 
				"{SELECT (count(?instance) as ?links) ?class {\n" + 
				"?class rdfs:subClassOf* ?superclass.\n" + 
				"?instance rdf:type ?class.\n" + 
				"?instance owl:sameAs ?instance1.\n" + 
				"FILTER (?superclass = <classuri>)\n" + 
				"} GROUP BY ?class}\n" + 
				"}";
		
//		String q = "SELECT ?superclass ?count ?links {\n" + 
//				"{SELECT ?superclass (count(distinct ?instance) as ?count) {\n" + 
//				"?class rdfs:subClassOf* ?superclass.\n" + 
//				"?instance rdf:type ?class.\n" + 
//				"FILTER (?superclass = <http://dbpedia.org/ontology/Group>)\n" + 
//				"}}\n" + 
//				"OPTIONAL\n" + 
//				"{SELECT ?superclass (count(distinct ?instance) as ?links) {\n" + 
//				"?class rdfs:subClassOf* ?superclass.\n" + 
//				"?instance rdf:type ?class.\n" + 
//				"?instance owl:sameAs ?instance1.\n" + 
//				"FILTER (?superclass = <http://dbpedia.org/ontology/Group>)\n" + 
//				"}}\n" + 
//				"}";
		
        ResultSet r = EngineQuery.SparqlQueryToModel(this.getPattern(), this.getModel());
        while (r.hasNext()) {
			QuerySolution qs1 = (QuerySolution) r.next();
			classes.add(qs1.getResource("?class").getURI().toString());
			classes.add(qs1.getResource("?superclass").getURI().toString());
		}
        
        logger.debug("classes = " +classes.toString());
        
        for (String oneclass : classes) {
			String stringQuery = q.replace("classuri", oneclass);
			logger.debug(stringQuery);
			Query query = QueryFactory.create();
			query.setPrefixMapping(PrefixMapping.Standard);
			QueryFactory.parse(query, stringQuery, "", Syntax.syntaxSPARQL_11);
			QueryExecution qexec = QueryExecutionFactory.sparqlService(this.getEndpoint(), query);
			ResultSet r1 = qexec.execSelect();
			while (r1.hasNext()) {
	        	HashMap<String, Double> cardi = new HashMap<String, Double>();
				QuerySolution qs2 = (QuerySolution) r1.next();
				cardi.put("count" , this.getOp() * (double) (qs2.getLiteral("?count") == null ? 0 : (int) qs2.getLiteral("?count").getValue()));
				cardi.put("links", this.getOp() * (double) (qs2.getLiteral("?links") == null ? 0 : (int) qs2.getLiteral("?links").getValue()));
				cardinalities.put(qs2.getResource("?class").getURI(), cardi);
			}
		}
        return cardinalities;
	}
	
	
}
