package fr.inrialpes.exmo.stream;

import java.util.HashMap;

import com.hp.hpl.jena.rdf.model.Model;
/**
 * 
 * @author asanchez75
 *
 */
public interface Triple {
	
	public void setPattern(String pattern);
	
	public String getPattern();
	
	public void setGraph(String graph);
	
	public String getGraph();
	
	public void setEndpoint(String endpoint);
	
	public String getEndpoint();
	
	public void setModel();
	
	public Model getModel();
	
	// to retrieve extra triples to build a model over which we will run a sparql query
	public Model getExtraTriples();
	
	// return array("classname" => array("count" => 10, "links" => 10))
	public HashMap<String, HashMap<String, Double>> computeCardinalities();
}
