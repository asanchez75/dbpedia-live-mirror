package fr.inrialpes.exmo.stream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class PairWiseComparisonMatrix {

	private static PairWiseComparisonMatrix instance;
	
	private void PairWiseComparisonMatrix() {}

    public static PairWiseComparisonMatrix getInstance() {
        if( instance == null ) {
            instance = new PairWiseComparisonMatrix();
        }
         return instance;
    }
	
	public HashMap<String, HashMap<String, Double>> getData() throws FileNotFoundException, IOException {
		HashMap<String, HashMap<String, Double>> rows = new HashMap<String, HashMap<String, Double>>();
		CSVParser records = new CSVParser(new FileReader(new File("matrix2014.csv")),CSVFormat.EXCEL.withHeader().withDelimiter('|'));
		for (CSVRecord csvRecord : records) {
			//System.out.println(csvRecord.get("A-uri") + "|" + csvRecord.get("A-total") + "|" + csvRecord.get("A-links"));
	       	HashMap<String, Double> cardi = new HashMap<String, Double>();
        	cardi.put("count", Double.parseDouble(csvRecord.get("A-total")));
        	cardi.put("links", Double.parseDouble(csvRecord.get("A-links")));
			rows.put(csvRecord.get("A-uri"), cardi);
		}
		return rows;
	}
	
	
	// example
	public static void main (String[] args) throws FileNotFoundException, IOException {
		PairWiseComparisonMatrix matrix = PairWiseComparisonMatrix.getInstance();
		System.out.println(matrix.getData());
	}
}
