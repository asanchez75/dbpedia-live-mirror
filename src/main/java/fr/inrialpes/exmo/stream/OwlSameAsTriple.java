package fr.inrialpes.exmo.stream;

import java.util.HashMap;

import org.dbpedia.extraction.live.mirror.LiveSync;
import org.slf4j.Logger;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.util.URIref;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;

/**
 * 
 * @author asanchez75
 *
 */
public class OwlSameAsTriple implements Triple {
	
	String pattern = "";
	String graph = "";
    String endpoint = "";
    Model model = null;
    Double op = 1.0;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(OwlSameAsTriple.class);
    
	public OwlSameAsTriple() {
		this.pattern = "SELECT ?s ?p ?o {?s ?p ?o. FILTER(?p = owl:sameAs)}";
	}

	@Override
	public Model getExtraTriples() {
		// for the local model
		String q = "SELECT ?instance ?p ?class {GRAPH <graphname> {\n" + 
				"?class ?p owl:Class.\n" + 
				"?instance ?p ?class.\n" + 
				"FILTER (?instance = <uri> && ?p = rdf:type)\n" + 
				"}}\n";

        ResultSet r = EngineQuery.SparqlQueryToModel(this.getPattern(), this.getModel());
        //ResultSetFormatter.out(r);
        while (r.hasNext()) {
        	QuerySolution qs1 = (QuerySolution) r.next();
        	logger.debug(qs1.get("?s").toString());
        	String stringQuery = q.replace("uri", URIref.decode(qs1.get("?s").toString())).replace("graphname", this.getGraph());
        	Query query = QueryFactory.create();
        	query.setPrefixMapping(PrefixMapping.Standard);
        	try {
        		logger.debug(stringQuery);
        		QueryFactory.parse(query, stringQuery, "", Syntax.syntaxSPARQL_11);
        		QueryExecution qexec = QueryExecutionFactory.sparqlService(this.getEndpoint(), query);
        		ResultSet r2 = qexec.execSelect();
        		//ResultSetFormatter.out(r);
        		while (r2.hasNext()) {
        			QuerySolution qs2 = (QuerySolution) r2.next();
        			Resource resource = this.getModel().createResource(URIref.encode(qs2.get("?instance").toString()));
        			this.getModel().add(resource, RDF.type, qs2.get("?class"));
        			this.getModel().add(qs2.getResource("?class"), RDF.type, OWL.Class);
        		}
			} catch (Exception e) {
				System.out.println(URIref.decode("This URI is misspelled: " + qs1.get("?s").toString()));
			}
		}
        return this.getModel();
	}
	
	@Override
	public String getPattern() {
		return this.pattern;
	}

	@Override
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public void setGraph(String graph) {
		this.graph = graph;
	}

	@Override
	public String getGraph() {
		return this.graph;
	}

	@Override
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	@Override
	public String getEndpoint() {
		return this.endpoint;
	}

	@Override
	public void setModel() {
		Model m = ModelFactory.createDefaultModel(); 
		this.model = m;
	}
	
	public void setModel(Model m) {
		this.model = m;
	}
	
	@Override
	public Model getModel() {
		return this.model;
	}
	
	public void setOp(Double op) {
		this.op = op;
	}
	
	public Double getOp(){
		return this.op;
	}
	@Override
	public HashMap<String, HashMap<String, Double>> computeCardinalities() {
		HashMap<String, HashMap<String, Double>> cardinalities = new HashMap<String, HashMap<String, Double>>();
		String stringQuery = "SELECT ?class ?count ?links { { SELECT ?class (count(DISTINCT ?instance) as ?count) { ?class rdf:type owl:Class. ?instance rdf:type ?class. } GROUP BY ?class } OPTIONAL { SELECT ?class (count(DISTINCT ?instance) as ?links) { ?class rdf:type owl:Class. ?instance rdf:type ?class. ?instance owl:sameAs ?anotherinstance. } GROUP BY ?class } } ";
		Query query = QueryFactory.create();
        query.setPrefixMapping(PrefixMapping.Standard);
        QueryFactory.parse(query, stringQuery, "", Syntax.syntaxSPARQL_11);
        QueryExecution qexec = QueryExecutionFactory.create(query, this.getModel());
        ResultSet r = qexec.execSelect();
        //ResultSetFormatter.out(r);
        while (r.hasNext()) {
			QuerySolution qs = (QuerySolution) r.next();
			if (qs.getResource("?class") != null) {
        	HashMap<String, Double> cardi = new HashMap<String, Double>();
        	cardi.put("count", this.getOp() * (double) (qs.getLiteral("?count") == null ? 0 : (int) qs.getLiteral("?count").getValue()));
        	cardi.put("links", this.getOp() * (double) (qs.getLiteral("?links") == null ? 0 : (int) qs.getLiteral("?links").getValue()));
			cardinalities.put(qs.get("?class").toString(), cardi);
			}
		}
        return cardinalities;
	}
	
	
}
