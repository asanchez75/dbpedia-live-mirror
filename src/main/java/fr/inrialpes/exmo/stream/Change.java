package fr.inrialpes.exmo.stream;

import java.awt.List;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.math3.util.Precision;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.dbpedia.extraction.live.mirror.helper.Utils;
import org.slf4j.Logger;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
/**
 * 
 * @author asanchez75
 * It serves as documentation but not as part of the program
 */
public class Change {
	
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(Change.class);
	public static HashMap<String, HashMap<String, Double>> mergeCardinalities(HashMap<String, HashMap<String, Double>> additions, HashMap<String, HashMap<String, Double>> deletions) {
		HashMap<String, HashMap<String, Double>> additions_clone = (HashMap<String, HashMap<String, Double>>) additions.clone();
		HashMap<String, HashMap<String, Double>> deletions_clone = (HashMap<String, HashMap<String, Double>>) deletions.clone();
		
		logger.info("additions_clone = " + additions_clone.toString());
		logger.info("deletions_clone = " + deletions_clone.toString());
		
		HashMap<String, HashMap<String, Double>> result = new HashMap<String, HashMap<String,Double>>();
		
    	for (Entry<String, HashMap<String, Double>> a : additions.entrySet()) {
    	   	for (Entry<String, HashMap<String, Double>> r : deletions.entrySet()) {
    	   		if (a.getKey().equals(r.getKey())) {
    	   			// it is a sum because the deletions have already the sign minus
					Double count = a.getValue().get("count") + r.getValue().get("count");
					Double links = a.getValue().get("links") + r.getValue().get("links");
					HashMap<String, Double> row = new HashMap<String, Double>();
					row.put("count", count);
					row.put("links", links);
					result.put(a.getKey(), row);
					// we remove from map to leave only classes not available in either added or removed
					additions_clone.remove(a.getKey());
					deletions_clone.remove(r.getKey());
				}
    	   	}
		}
    	// to add the remainder
		result.putAll(additions_clone);
		result.putAll(deletions_clone);
    	return result;
	}
	
	/**
	 * Compares coming changes (cardinalities) against pairwise comparison matrix
	 * @param changes
	 * @param matrix
	 * @return
	 */
	public static HashMap<String, HashMap<String, Double>> evolution(HashMap<String, HashMap<String, Double>> changes, HashMap<String, HashMap<String, Double>> matrix) {
		HashMap<String, HashMap<String, Double>> result = new HashMap<String, HashMap<String, Double>>();
		HashMap<String, HashMap<String, Double>> changes_clone = (HashMap<String, HashMap<String, Double>>) changes.clone();
		
		for (Entry<String, HashMap<String, Double>> change : changes.entrySet()) {
			for (Entry<String, HashMap<String, Double>>  row : matrix.entrySet()) {
    	   		if (change.getKey().equals(row.getKey())) {
    	   			// includes only if either count or links varies 
    	   			if (change.getValue().get("count") != 0  && row.getValue().get("count") != 0) {
        	   			// always is a sum because the variable count comes with minus sign
    					double count = (double) (row.getValue().get("count") + change.getValue().get("count")) / (double) row.getValue().get("count");
    				    double links = (double) (row.getValue().get("links") + change.getValue().get("links")) / (double) row.getValue().get("links");
    			       	HashMap<String, Double> cardi = new HashMap<String, Double>();
    		        	cardi.put("%count", Precision.round(count, 3));
    		        	cardi.put("%links", Precision.round(links, 3));
    		        	result.put(change.getKey(), cardi);
    					// we remove from map to leave only classes not available 
    		        	changes_clone.remove(change.getKey());
					}
    	   		}
			}
		}
    	// to add the remainder
		result.putAll(changes_clone);		
		return result;
	}
}
