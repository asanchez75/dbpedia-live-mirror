package fr.inrialpes.exmo.stream;

import java.awt.List;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.dbpedia.extraction.live.mirror.helper.Utils;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

public class TripleParser {
    
	private Collection<String> triples;
	private StmtIterator parsed_triples;
	HashMap<String, String> buffer = new HashMap<String, String>();
	public Model model = null;
	public static String lang = "N-TRIPLES";
	public static String added = "changeset/added.nt";
	private static TripleParser instance;
	public HashMap<String, HashMap<String, Double>> cardinalities = new HashMap<String, HashMap<String, Double>>();
	
	private void TripleParser() {}

    public static TripleParser getInstance() {
        if( instance == null ) {
            instance = new TripleParser();
        }
         return instance;
    }
    
	public void setSource(Collection<String> triples) {
		this.triples = triples;
	}	

	public void parse() {
    	if (this.model == null) {
        	Model model = ModelFactory.createDefaultModel();
        	this.model = model;
		}

    	String data = Utils.generateStringFromList(this.triples, "");
    	InputStream inputStream = new ByteArrayInputStream(data.getBytes(Charset.forName("UTF-8")));
    	//RDFDataMgr.read(model, inputStream, Lang.NTRIPLES);
    	RDFDataMgr.read(this.model, inputStream, Lang.NTRIPLES) ;

    }
	
    public void findRdfTypeTriple() {
		RdfTypeTriple triple = new RdfTypeTriple();
		triple.setEndpoint("http://37.187.144.161:8890/sparql");
		triple.setGraph("http://dbpedia.org");
		triple.setModel(this.model);
		Model triples = triple.getExtraTriples();
		this.cardinalities = this.mergeCardinalities(this.cardinalities, triple.computeCardinalities());
    }
    
    public void findOwlSameAsTriple() {
		OwlSameAsTriple triple = new OwlSameAsTriple();
		triple.setEndpoint("http://37.187.144.161:8890/sparql");
		triple.setGraph("http://dbpedia.org");
		triple.setModel(this.model);
		Model triples = triple.getExtraTriples();
		this.cardinalities = this.mergeCardinalities(this.cardinalities, triple.computeCardinalities());
    }
    
	public void findRdfsSubClassOfTriple() {
		RdfsSubClassOfTriple triple = new RdfsSubClassOfTriple();
		triple.setEndpoint("http://37.187.144.161:8890/sparql");
		triple.setGraph("http://dbpedia.org");
		triple.setModel(this.model);
		Model triples = triple.getExtraTriples();
		this.cardinalities = this.mergeCardinalities(this.cardinalities, triple.computeCardinalities());
	}
	
	// to think
	private HashMap<String, HashMap<String, Double>> mergeCardinalities(HashMap<String, HashMap<String, Double>> currentCardinalities, HashMap<String, HashMap<String, Double>> newCardinalities) {

		HashMap<String, HashMap<String, Double>> currentCardinalities_clone = (HashMap<String, HashMap<String, Double>>) currentCardinalities.clone();
		HashMap<String, HashMap<String, Double>> newCardinalities_clone = (HashMap<String, HashMap<String, Double>>) newCardinalities.clone();
		
		HashMap<String, HashMap<String, Double>> merge = new HashMap<String, HashMap<String,Double>>();
		
    	for (Entry<String, HashMap<String, Double>> a : currentCardinalities.entrySet()) {
    	   	for (Entry<String, HashMap<String, Double>> r : newCardinalities.entrySet()) {
    	   		if (a.getKey().equals(r.getKey())) {
					Double count = a.getValue().get("count") + r.getValue().get("count");
					Double links = a.getValue().get("links") + r.getValue().get("links");
					HashMap<String, Double> row = new HashMap<String, Double>();
					row.put("count", count);
					row.put("links", links);
					merge.put(a.getKey(), row);
					// we remove from map to leave only classes not available in either added or removed
					currentCardinalities_clone.remove(a.getKey());
					newCardinalities_clone.remove(r.getKey());
				}
    	   	}
		}
    	// to add the remainder
		merge.putAll(currentCardinalities_clone);
		merge.putAll(newCardinalities_clone);
    	return merge;
		
	}
}
