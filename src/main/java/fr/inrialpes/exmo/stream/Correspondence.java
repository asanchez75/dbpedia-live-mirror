package fr.inrialpes.exmo.stream;

public class Correspondence {
	
	String sourceClass;
	Double sourceClassSize;
	Double sourceClassSameasLinks;
	String targetClass;
	Double targetClassSize;	
	Double targetClassSameasLinks;
	Double intersection;
	
	
	public String getSourceClass() {
		return sourceClass;
	}
	public void setSourceClass(String sourceClass) {
		this.sourceClass = sourceClass;
	}
	public Double getSourceClassSize() {
		return sourceClassSize;
	}
	public void setSourceClassSize(Double sourceClassSize) {
		this.sourceClassSize = sourceClassSize;
	}
	public Double getSourceClassSameasLinks() {
		return sourceClassSameasLinks;
	}
	public void setSourceClassSameasLinks(Double sourceClassSameasLinks) {
		this.sourceClassSameasLinks = sourceClassSameasLinks;
	}
	public String getTargetClass() {
		return targetClass;
	}
	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}
	public Double getTargetClassSize() {
		return targetClassSize;
	}
	public void setTargetClassSize(Double targetClassSize) {
		this.targetClassSize = targetClassSize;
	}
	public Double getTargetClassSameasLinks() {
		return targetClassSameasLinks;
	}
	public void setTargetClassSameasLinks(Double targetClassSameasLinks) {
		this.targetClassSameasLinks = targetClassSameasLinks;
	}	

	
	
}
