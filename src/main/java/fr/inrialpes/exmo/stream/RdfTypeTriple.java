package fr.inrialpes.exmo.stream;

import java.util.HashMap;

import org.slf4j.Logger;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFactory;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.query.ResultSetRewindable;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.util.URIref;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
/**
 * 
 * @author asanchez75
 *
 */
public class RdfTypeTriple implements Triple {
	
	String pattern = "";
	String graph = "";
    String endpoint = "";
    Model model = null;
    // for additions = 1, for deletions = -1
    Double op = 1.0;
    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(RdfTypeTriple.class);
    
	public RdfTypeTriple() {
		this.pattern = "SELECT ?s ?p ?o {\n" + 
				"?s ?p ?o.\n" + 
				"FILTER (?p = rdf:type)\n" + 
				"}\n";
	}

	@Override
	public Model getExtraTriples() {
		// for the remote sparql endpoint
		String q2 = "SELECT ?superclass ?p ?o {\n" + 
				"GRAPH <graphname> {\n" + 
				"?class rdfs:subClassOf* ?superclass.\n" + 
				"?class ?p ?o. \n" + 
				"FILTER (?class = <uri> && ?p = rdf:type && ?o = owl:Class)\n" + 
				"}\n" + 
				"}";

        ResultSet r = EngineQuery.SparqlQueryToModel(this.getPattern(), this.getModel());
        ResultSetRewindable s = ResultSetFactory.makeRewindable(r);

        while (s.hasNext()) {
			QuerySolution querySolution = (QuerySolution) s.next();
			String query = q2.replace("uri", URIref.decode(querySolution.get("?o").toString())).replace("graphname", this.getGraph());
	        ResultSet r3 = EngineQuery.SparqlQueryToEndpoint(query, this.getEndpoint());
	        ResultSetRewindable w = ResultSetFactory.makeRewindable(r3);
	        while (w.hasNext()) {
				QuerySolution querySolution3 = (QuerySolution) w.next();
				Resource resource = this.getModel().createResource(URIref.encode(querySolution.get("?s").toString()));
				this.getModel().add(resource, RDF.type, querySolution3.get("?superclass"));
				this.getModel().add(querySolution3.getResource("?superclass"), RDF.type, OWL.Class);
			}

		}
        return this.getModel();
	}
	
	@Override
	public String getPattern() {
		return this.pattern;
	}

	@Override
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public void setGraph(String graph) {
		this.graph = graph;
	}

	@Override
	public String getGraph() {
		return this.graph;
	}

	@Override
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	@Override
	public String getEndpoint() {
		return this.endpoint;
	}

	@Override
	public void setModel() {
		Model m = ModelFactory.createDefaultModel(); 
		this.model = m;
	}
	
	public void setModel(Model m) {
		this.model = m;
	}
	
	@Override
	public Model getModel() {
		return this.model;
	}
	
	public void setOp(Double op) {
		this.op = op;
	}
	
	public Double getOp(){
		return this.op;
	}

	@Override
	public HashMap<String, HashMap<String, Double>> computeCardinalities() {
		HashMap<String, HashMap<String, Double>> cardinalities = new HashMap<String, HashMap<String, Double>>();
		String stringQuery = "SELECT ?class ?count ?links { { SELECT ?class (count(DISTINCT ?instance) as ?count) { ?class rdf:type owl:Class. ?instance rdf:type ?class. } GROUP BY ?class } OPTIONAL { SELECT ?class (count(DISTINCT ?instance) as ?links) { ?class rdf:type owl:Class. ?instance rdf:type ?class. ?instance owl:sameAs ?anotherinstance. } GROUP BY ?class } } ";
		Query query = QueryFactory.create();
        query.setPrefixMapping(PrefixMapping.Standard);
        QueryFactory.parse(query, stringQuery, "", Syntax.syntaxSPARQL_11);
        QueryExecution qexec = QueryExecutionFactory.create(query, this.getModel());
        ResultSet r = qexec.execSelect();
        //ResultSetFormatter.out(r);
        while (r.hasNext()) {
			QuerySolution qs = (QuerySolution) r.next();
			if (qs.getResource("?class") != null) {
	        	HashMap<String, Double> cardi = new HashMap<String, Double>();
	        	cardi.put("count", this.getOp() * (double) (qs.getLiteral("?count") == null ? 0 : (int) qs.getLiteral("?count").getValue()));
	        	cardi.put("links", this.getOp() * (double) (qs.getLiteral("?links") == null ? 0 : (int) qs.getLiteral("?links").getValue()));
				cardinalities.put(qs.get("?class").toString(), cardi);				
			}
		}
        return cardinalities;
	}
	
	
}
