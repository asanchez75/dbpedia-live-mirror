package fr.inrialpes.exmo.stream;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;

import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.dbpedia.extraction.live.mirror.changesets.ChangesetCounter;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

public class MyTest {
	public static String lang = "N-TRIPLES";
	public static String added = "changeset/added.nt";

	public static void main(String[] args) throws FileNotFoundException {
		//PrintStream out = new PrintStream(new FileOutputStream("output.txt"));
		//System.setOut(out);
		testRdfTypeTriple();
		//testOwlSameAsTriple();
		//RdfsSubClassOfTriple();
		//myComparison();
	}
	
	public static void testRdfTypeTriple() {
		RdfTypeTriple triple = new RdfTypeTriple();
		triple.setEndpoint("http://37.187.144.161:8890/sparql");
		triple.setGraph("http://dbpedia.org");
		Model model = ModelFactory.createDefaultModel(); 
		model.read(added, lang);
		triple.setModel(model);
		Model triples = triple.getExtraTriples();
		StmtIterator it = triples.listStatements();
		while (it.hasNext()) {
			Statement st = (Statement) it.next();
			System.out.println(st);
		}
		System.out.println(triple.computeCardinalities());
	}

	public static void testOwlSameAsTriple() {
		OwlSameAsTriple triple = new OwlSameAsTriple();
		triple.setEndpoint("http://37.187.144.161:8890/sparql");
		triple.setGraph("http://dbpedia.org");
		Model model = ModelFactory.createDefaultModel(); 
		model.read(added, lang);
		triple.setModel(model);
		Model triples = triple.getExtraTriples();
		StmtIterator it = triples.listStatements();
		while (it.hasNext()) {
			Statement st = (Statement) it.next();
			System.out.println(st);
		}
		System.out.println(triple.computeCardinalities());
	}
	
	public static void RdfsSubClassOfTriple() {
		RdfsSubClassOfTriple triple = new RdfsSubClassOfTriple();
		triple.setEndpoint("http://37.187.144.161:8890/sparql");
		triple.setGraph("http://dbpedia.org");
		Model model = ModelFactory.createDefaultModel(); 
		model.read(added, lang);
		triple.setModel(model);
		System.out.println(triple.computeCardinalities());

	}
	
	public static void myComparison(){
		int num1 = 14;
		int num2 = 5;
		if (Integer.valueOf(num1).compareTo(Integer.valueOf(num2)) > 0) {
			System.out.println("x");
		}
		else {
			System.out.println("y");
		}
	}
	
	public static void testReadRdfFile() {
		// TODO Auto-generated method stub
	String data  = "<http://data.linkedmdb.org/resource/film/9993>	<http://xmlns.com/foaf/0.1/page>	<http://www.imdb.com/title/tt0159259/> . <http://data.linkedmdb.org/resource/film/98791>	<http://data.linkedmdb.org/resource/movie/director>	<http://data.linkedmdb.org/resource/director/11015> . ";
	Model model = ModelFactory.createDefaultModel();
	InputStream inputStream = new ByteArrayInputStream(data.getBytes(Charset.forName("UTF-8")));
	RDFDataMgr.read(model, inputStream, Lang.NTRIPLES) ;
	StmtIterator results = model.listStatements();
	while (results.hasNext()) {
	  System.out.println(results.next().getPredicate());
	}
	
	}
	
	public static void testModel() {
		Model model = ModelFactory.createDefaultModel();
		model.read("model.ttl", "TURTLE");
		StmtIterator results = model.listStatements();
		  while (results.hasNext()) {
		    System.out.println(results.next());
		  }
	}
	
	public static void testChangesetCounter() {
		ChangesetCounter o = new ChangesetCounter(2015, 07, 01, 18, 1);
		System.out.println("1 \t" + o.toString());
		o.advancePatch();
		System.out.println(o.getFormattedFilePath());
		System.out.println("3 \t" + o.toString());		
		o.advanceHour();
		System.out.println(o.getFormattedFilePath());
		System.out.println("2 \t" + o.toString());
	}

}
