package fr.inrialpes.exmo.stream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.dbpedia.extraction.live.mirror.helper.Utils;
import org.slf4j.Logger;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class SimpleSync {
    
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SimpleSync.class);
    
	public String endpoint = "http://37.187.144.161:8890/sparql";
	public String graphname = "http://dbpedia.org";
	public String lang = "N-TRIPLES";
	public static String triplesAdded = "changeset/added.nt";
	public static String triplesRemoved = "changeset/removed.nt";
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		List<String> added = Utils.getTriplesFromFile(triplesAdded);
        TripleParser additions = AdditionsTripleParser.getInstance();
        additions.setSource(added);
        additions.parse();
        additions.findRdfTypeTriple();
        additions.findOwlSameAsTriple();
        //additions.findRdfsSubClassOfTriple();

        
		List<String> removed = Utils.getTriplesFromFile(triplesRemoved);
        TripleParser deletions = DeletionsTripleParser.getInstance();
        deletions.setSource(removed);
        deletions.parse();
        deletions.findRdfTypeTriple();
        deletions.findOwlSameAsTriple();
        //deletions.findRdfsSubClassOfTriple();
 		
    	// when the process finishes merge both results from additions and deletions
    	HashMap<String, HashMap<String, Double>> changes = Change.mergeCardinalities(additions.cardinalities, deletions.cardinalities);
    	// loading pairwise comparison matrix
		PairWiseComparisonMatrix matrix = PairWiseComparisonMatrix.getInstance();
		// computing evolution
		logger.info("changes = " + changes.toString());
		HashMap<String, HashMap<String, Double>> result = Change.evolution(changes, matrix.getData());
    	
		logger.info("final result: " + result.toString());
	}
	
}
