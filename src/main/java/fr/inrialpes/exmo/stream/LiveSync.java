package fr.inrialpes.exmo.stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Precision;
import org.dbpedia.extraction.live.mirror.changesets.Changeset;
import org.dbpedia.extraction.live.mirror.changesets.ChangesetCounter;
import org.dbpedia.extraction.live.mirror.changesets.ChangesetExecutor;
import org.dbpedia.extraction.live.mirror.download.LastDownloadDateManager;
import org.dbpedia.extraction.live.mirror.helper.Global;
import org.dbpedia.extraction.live.mirror.helper.UpdateStrategy;
import org.dbpedia.extraction.live.mirror.helper.Utils;
import org.dbpedia.extraction.live.mirror.sparul.JDBCPoolConnection;
import org.dbpedia.extraction.live.mirror.sparul.SPARULGenerator;
import org.dbpedia.extraction.live.mirror.sparul.SPARULVosExecutor;
import org.slf4j.Logger;

import com.fasterxml.jackson.core.io.MergedStream;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

/**
 * Created by IntelliJ IDEA.
 * User: Mohamed Morsey
 * Modified by: Adam Sanchez
 * Date: 5/24/11
 * Time: 4:26 PM
 * This class is originally created from class defined in http://www.devdaily.com/java/edu/pj/pj010011
 * which is created by http://www.DevDaily.com
 */
public final class LiveSync {


    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(LiveSync.class);

    private static final String LAST_DOWNLOAD = "lastDownloadDate.dat";
    private static final int ERRORS_TO_ADVANCE = 3;

    private static final String EXTENSION_ADDED =  ".added.nt.gz";
    private static final String EXTENSION_REMOVED =  ".removed.nt.gz";
    private static final String EXTENSION_CLEAR = ".clear.nt.gz";
    private static final String EXTENSION_REINSERT = ".reinserted.nt.gz ";
    
    public static final String START_TIME_WINDOW = "2014-11-11-07-000001";
    public static final String END_TIME_WINDOW = "2014-11-11-07-000002";    

    private LiveSync() {
    }

    public static void main(String[] args) throws IOException {

        if (args != null && args.length > 1) {
            logger.error("Incorrect arguments in Main. Must be zero or one of {Endless|Onetime}");
            System.exit(1);
        }
        UpdateStrategy strategy = UpdateStrategy.Onetime; // default value
        if (args != null && args.length == 1) {
            try {
                strategy = UpdateStrategy.valueOf(args[0]);
            } catch (Exception e) {
                logger.error("Incorrect arguments in Main. Must be one of {Endless|Onetime}");
                System.exit(1);
            }
        }
       
        // Variable init from ini file
        String updateServerAddress = Global.getOptions().get("UpdateServerAddress");
        String updatesDownloadFolder = Global.getOptions().get("UpdatesDownloadFolder");
        long delayInSeconds = Long.parseLong(Global.getOptions().get("LiveUpdateInterval")) * 1000l;

        ChangesetCounter startCounter = new ChangesetCounter(START_TIME_WINDOW);
        startCounter.advancePatch(); // move to next patch (this one is already applied

        ChangesetCounter endCounter = new ChangesetCounter(END_TIME_WINDOW);        

        int missing_urls = 0;
        
        while (true) {

            TripleParser deletions = DeletionsTripleParser.getInstance();
            TripleParser additions = AdditionsTripleParser.getInstance();
            
            // when we are about to go beyond the remote published file
        	// if in some moment during the iteration, startCounter surpasses endCounter then...
            if (startCounter.compareTo(endCounter) > 0) {
            	
            	// when the process finishes merge both results from additions and deletions
            	HashMap<String, HashMap<String, Double>> changes = Change.mergeCardinalities(additions.cardinalities, deletions.cardinalities);
            	// loading pairwise comparison matrix
        		PairWiseComparisonMatrix matrix = PairWiseComparisonMatrix.getInstance();
        		// computing evolution
        		logger.info("changes = " + changes.toString());
        		HashMap<String, HashMap<String, Double>> result = Change.evolution(changes, matrix.getData());
            	
        		logger.info("final result: " + result.toString());
        		
                /**
                 * TODO between the app started (or last fetch of last published file)
                 * the remote counter may be advanced but we don't take this into consideration here
                 * probably should re-download in a temp counter, check if different and continue without sleep
                 */

                // in case of onetime run, exit
                if (strategy.equals(UpdateStrategy.Onetime)) {
                    logger.info("Finished the One-Time update, exiting...");
                    break;
                }

                // sleep + download new file & continue
                logger.info("Up-to-date with last published changeset, sleeping for a while ;)");
                try {
                    Thread.sleep(delayInSeconds);
                } catch (InterruptedException e) {
                    logger.warn("Could not sleep...", e);
                }

                endCounter = new ChangesetCounter(END_TIME_WINDOW);
                //now we have an updated remote counter so next time this block will not run (if the updates are running)
                continue;

            }

            String addedTriplesURL = updateServerAddress + startCounter.getFormattedFilePath() + EXTENSION_ADDED;
            String deletedTriplesURL = updateServerAddress + startCounter.getFormattedFilePath() + EXTENSION_REMOVED;
            String clearTriplesURL = updateServerAddress + startCounter.getFormattedFilePath() + EXTENSION_CLEAR;
            String reinsertTriplesURL = updateServerAddress + startCounter.getFormattedFilePath() + EXTENSION_REINSERT;

            // changesets default to empty
            List<String> triplesToDelete = Arrays.asList();
            List<String> triplesToAdd = Arrays.asList();
            List<String> resourcesToClear = new LinkedList<>();
            List<String> triplesToReInsert = new LinkedList<>();

            //Download and decompress the file of deleted triples
            String addedCompressedDownloadedFile = Utils.downloadFile(addedTriplesURL, updatesDownloadFolder);
            String deletedCompressedDownloadedFile = Utils.downloadFile(deletedTriplesURL, updatesDownloadFolder);
            String clearCompressedDownloadedFile = Utils.downloadFile(clearTriplesURL, updatesDownloadFolder);
            String reinsertCompressedDownloadedFile = Utils.downloadFile(reinsertTriplesURL, updatesDownloadFolder);

            // Check for errors before proceeding
            if (addedCompressedDownloadedFile == null && deletedCompressedDownloadedFile == null && clearCompressedDownloadedFile == null && reinsertCompressedDownloadedFile == null) {
                missing_urls++;
                if (missing_urls >= ERRORS_TO_ADVANCE) {
                    // advance hour / day / month or year
                    startCounter.advanceHour();
                }
                continue;
            }

            // URL works, reset missing URLs
            missing_urls = 0;

            if (clearCompressedDownloadedFile != null) {

                String file = Utils.decompressGZipFile(clearCompressedDownloadedFile);
                List<String> temp_triples = Utils.getTriplesFromFile(file);
                for (String triple : temp_triples) {
                    String[] splittedTriple = triple.split("> <");
                    String tmp_resource = splittedTriple[0];
                    String resource = tmp_resource.substring(1);
                    resourcesToClear.add(resource);
                }
                Utils.deleteFile(file);
            }

            if (deletedCompressedDownloadedFile != null) {

                String file = Utils.decompressGZipFile(deletedCompressedDownloadedFile);
                triplesToDelete = Utils.getTriplesFromFile(file);
                //System.out.println("\ntodelete\n" +triplesToDelete);
                Utils.deleteFile(file);
            }


            if (addedCompressedDownloadedFile != null) {
                String decompressedAddedNTriplesFile = Utils.decompressGZipFile(addedCompressedDownloadedFile);
                triplesToAdd = Utils.getTriplesFromFile(decompressedAddedNTriplesFile);
                Utils.deleteFile(decompressedAddedNTriplesFile);
            }

            if (reinsertCompressedDownloadedFile != null) {
                String decompressedReInsertNTriplesFile= Utils.decompressGZipFile(reinsertCompressedDownloadedFile);
                triplesToReInsert = Utils.getTriplesFromFile(decompressedReInsertNTriplesFile);

                Utils.deleteFile(decompressedReInsertNTriplesFile);
            }

            Changeset changeset = new Changeset(startCounter.toString(), triplesToAdd, triplesToDelete, resourcesToClear, triplesToReInsert);

            deletions.setSource(changeset.getDeletions());
            deletions.parse();
            deletions.findRdfTypeTriple();
            deletions.findRdfsSubClassOfTriple();
            deletions.findOwlSameAsTriple();
            logger.info("deletions = " + deletions.cardinalities);

            additions.setSource(changeset.getAdditions());
            additions.parse();
            additions.findRdfTypeTriple();
            additions.findRdfsSubClassOfTriple();
            additions.findOwlSameAsTriple();
            logger.info("additions" + additions.cardinalities);

            // save last processed date
            LastDownloadDateManager.writeLastDownloadDate(LAST_DOWNLOAD, startCounter.toString());

            // advance to the next patch
            startCounter.advancePatch();

        }

    }  // end of main
    


}
