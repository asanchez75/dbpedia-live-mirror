package fr.inrialpes.exmo.stream;

import java.util.HashMap;
import java.util.Map.Entry;

import com.hp.hpl.jena.rdf.model.Model;

public class DeletionsTripleParser  extends TripleParser {
	private static DeletionsTripleParser instance;
	private void DeletionsTripleParser() {}
	
    public static DeletionsTripleParser getInstance() {
        if( instance == null ) {
            instance = new DeletionsTripleParser();
        }
         return instance;
    }
    public void findRdfTypeTriple() {
		RdfTypeTriple triple = new RdfTypeTriple();
		triple.setEndpoint("http://37.187.144.161:8890/sparql");
		triple.setGraph("http://dbpedia.org");
		triple.setModel(this.model);
		Model triples = triple.getExtraTriples();
		triple.setOp(-1.0);
		this.cardinalities = this.mergeCardinalities(this.cardinalities, triple.computeCardinalities());
    }
    
    public void findOwlSameAsTriple() {
		OwlSameAsTriple triple = new OwlSameAsTriple();
		triple.setEndpoint("http://37.187.144.161:8890/sparql");
		triple.setGraph("http://dbpedia.org");
		triple.setModel(this.model);
		Model triples = triple.getExtraTriples();
		triple.setOp(-1.0);
		this.cardinalities = this.mergeCardinalities(this.cardinalities, triple.computeCardinalities());
    }
    
	public void findRdfsSubClassOfTriple() {
		RdfsSubClassOfTriple triple = new RdfsSubClassOfTriple();
		triple.setEndpoint("http://37.187.144.161:8890/sparql");
		triple.setGraph("http://dbpedia.org");
		triple.setModel(this.model);
		Model triples = triple.getExtraTriples();
		triple.setOp(-1.0);
		this.cardinalities = this.mergeCardinalities(this.cardinalities, triple.computeCardinalities());
	}
	// to think
	private HashMap<String, HashMap<String, Double>> mergeCardinalities(HashMap<String, HashMap<String, Double>> currentCardinalities, HashMap<String, HashMap<String, Double>> newCardinalities) {

		HashMap<String, HashMap<String, Double>> currentCardinalities_clone = (HashMap<String, HashMap<String, Double>>) currentCardinalities.clone();
		HashMap<String, HashMap<String, Double>> newCardinalities_clone = (HashMap<String, HashMap<String, Double>>) newCardinalities.clone();
		
		HashMap<String, HashMap<String, Double>> merge = new HashMap<String, HashMap<String,Double>>();
		
    	for (Entry<String, HashMap<String, Double>> a : currentCardinalities.entrySet()) {
    	   	for (Entry<String, HashMap<String, Double>> r : newCardinalities.entrySet()) {
    	   		if (a.getKey().equals(r.getKey())) {
					Double count = a.getValue().get("count") + r.getValue().get("count");
					Double links = a.getValue().get("links") + r.getValue().get("links");
					HashMap<String, Double> row = new HashMap<String, Double>();
					row.put("count", count);
					row.put("links", links);
					merge.put(a.getKey(), row);
					// we remove from map to leave only classes not available in either added or removed
					currentCardinalities_clone.remove(a.getKey());
					newCardinalities_clone.remove(r.getKey());
				}
    	   	}
		}
    	// to add the remainder
		merge.putAll(currentCardinalities_clone);
		merge.putAll(newCardinalities_clone);
    	return merge;
		
	}
}
